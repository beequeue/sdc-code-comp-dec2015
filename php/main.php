<?php

$start = microtime(1);

// Find all 3-digit multiples of 28 that don't contain 0 and have unique digits
$i = 4;
$mult28 = [];
$mult = $i * 28;
while ($mult < 1000) {
	$mult = (string)$mult;
	$multArr = str_split($mult);
	list($x, $y, $z) = $multArr;
	if (strpos($mult, '0') === false && $x != $y && $y != $z && $x != $z) {
		$mult28[] = $mult;
	}
	$i++;
	$mult = $i * 28;
}

// Find all 3 digits multiples of 30 that have unique digits
// Group by first digit (A) because that's what we'll be interested in later
$i = 4;
$mult30 = [];
$mult = $i * 30;
while ($mult < 1000) {
	$mult = (string)$mult;
	list($x, $y, $z) = str_split($mult);
	if ($x != $y && $y != $z && $x != $z) {
		$mult30[$x][] = $mult;
	}
	$i++;
	$mult = $i * 30;
}


// Find all 3-digit multiples of 31 that don't contain 0 and have unique digits
// Group by middle digit because that's what we're interested in
$i = 4;
$mult31 = [];
$mult = $i * 31;
while ($mult < 1000) {
	$mult = (string)$mult;
	list($x, $y, $z) = str_split($mult);
	if (strpos($mult, '0') === false && $x != $y && $y != $z && $x != $z) {
	//if (strpos($mult, '0') === false && count(array_unique(str_split($mult))) == 3) {
		// Group by middle digit for later analysis
		//$midDigit = substr($mult, 1, 1);
		$mult31[$y][] = $mult;
	}
	$i++;
	$mult = $i * 31;
}

$allDigits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

foreach ($mult31 as $midDigit => $m) {
	// Must be a pair to satisfy both having a middle 'A'
	if (count($m) != 2) {
		continue;
	}

	// Digits 1 and 3 must be unique across the pair (MaY, JaN), including middle digit
	$digits = [$m[0][0], $m[0][2], $m[1][0], $m[1][2], $midDigit];
	if (count(array_unique($digits)) == 5) {

		// We need to check that there is a candidate APR whose second digit doesn't
		// occur in the current MAY/JUN multiples
		foreach ($mult30[$midDigit] as $m30) {
			$m30mid = substr($m30, 1, 1);
			if (!in_array($m30mid, $digits)) {

				// We know about the R = 0, so let's add that and our mid-digit
				$testDigits = $digits;
				$testDigits[] = '0';
				$testDigits[] = $m30mid;

				// We now need to check that there exists a candidate FEB that is
				// made up of the remaining digits
				$remainingDigits = array_diff($allDigits, $testDigits);
				foreach ($mult28 as $k => $m28) {
					if (in_array($m28[0], $remainingDigits)
						&& in_array($m28[1], $remainingDigits)
						&& in_array($m28[2], $remainingDigits)) {
						echo "$m28\n";
						break(3);
					}
				}
			}
		}
	};
}

echo ((microtime(1) - $start) * 1000000), "\n";
