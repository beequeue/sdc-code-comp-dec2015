#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

main()
{
	clock_t tic = clock();
	int i, j, mult;
	char mStr[4];

	// Find all 3-digit multiples of 28 that don't contain 0 and have unique digits
	char mult28[50][4];
	int m28MaxIdx = -1;
	i = 4;
	mult = i * 28;
	while (mult < 1000) {
		sprintf(mStr, "%d", mult);
		if (mStr[0] != mStr[1] && mStr[0] != mStr[2] && mStr[1] != mStr[2]
			&& mStr[0] != '0' && mStr[1] != '0' && mStr[2] != '0' ) {
			//printf("%s\n", mStr);
			strcpy(mult28[++m28MaxIdx], mStr);
		} else {
			//printf("%s - INVALID\n", mStr);
		}
		i++;
		mult = i * 28;
	}

	//printf("m28MaxIdx = %d\n", m28MaxIdx);


	// Find all 3 digit multiples of 30 that have unique digits
	char mult30[50][4];
	int m30MaxIdx = -1;
	i = 4;
	mult = i * 30;
	while (mult < 1000) {
		sprintf(mStr, "%d", mult);
		if (mStr[0] != mStr[1] && mStr[0] != mStr[2] && mStr[1] != mStr[2]) {
			//printf("%s\n", mStr);
			strcpy(mult30[++m30MaxIdx], mStr);
		} else {
			//printf("%s - INVALID\n", mStr);
		}
		i++;
		mult = i * 30;
	}

	//printf("m30MaxIdx = %d\n", m30MaxIdx);

	// Find all 3-digit multiples of 31 that don't contain 0 and have unique digits
	// Group by middle digit because that's what we're interested in
	char mult31[10][50][4];
	int mid;
	int m31MaxIdx[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	i = 4;
	mult = i * 31;
	while (mult < 1000) {
		sprintf(mStr, "%d", mult);
		mid = mStr[1] - '0'; // convert char digit to int
		//printf("Mid = %d\n", mid);
		if (mStr[0] != mStr[1] && mStr[0] != mStr[2] && mStr[1] != mStr[2]
			&& mStr[0] != '0' && mStr[1] != '0' && mStr[2] != '0' ) {
			//printf("%s\n", mStr);
			strcpy(mult31[mid][++m31MaxIdx[mid]], mStr);
		} else {
			//printf("%s - INVALID\n", mStr);
		}
		i++;
		mult = i * 31;
	}

	// Iterate the possible mid-digit groupings of mult31
	char digits[10], rDigits[3], midDigit, m30mid, f, e, b;
	int digitCounts[10], d, c, foundDup, isInArray;
	for (mid = 1; mid < 10; mid++) {
		// Must be a pair to satisfy both having a middle 'A'
		if (m31MaxIdx[mid] != 1) {
			continue;
		}

		midDigit = '0' + mid;

		//printf("-----------\nPossible mid: %d (%s, %s)\n", mid, mult31[mid][0], mult31[mid][1]);

		// Digits 1 and 3 must be unique across the pair (MaY, JaN), including middle digit
		digits[0] = mult31[mid][0][0];
		digits[1] = mult31[mid][0][2];
		digits[2] = mult31[mid][1][0];
		digits[3] = mult31[mid][1][2];
		digits[4] = midDigit;
		digits[5] = '\0';

		//printf("digits = %s\n", digits);

		// Oh array_unique, where art thou...
		memset(digitCounts, 0, sizeof(digitCounts));
 		foundDup = 0;
		for (i = 0; i < 5; i++) {
			d = digits[i] - '0';
     		if (!(digitCounts[d])) {
     			digitCounts[d]++;
     		} else {
          		// Duplicate found
         		//printf("Duplicate: %d\n", d);
         		foundDup = 1;
         		break;
         	}
		}
		if (foundDup) {
			continue;
		}

		// We need to check that there is a candidate APR whose second digit doesn't
		// occur in the current MAY/JUN multiples
		for (i = 0; i <= m30MaxIdx; i++) {
			m30mid = mult30[i][1];
			digits[5] = m30mid;;

			//printf("Candidate APR = %s\n", mult30[i]);
			isInArray = 0;
			if (mult30[i][0] != midDigit) { // Check the A
				isInArray = 1;
			} else {
				for (j = 0; j < 5; j++) {
					if (m30mid == digits[j]) {
						isInArray = 1;
						//printf("Already in digits: %c\n", m30mid);
						break;
					}
				}
			}
			if (!isInArray) {
				//printf("NOT in digits: %c\n", m30mid);

				// We know about the R = 0, so let's add that and our mid-digit
				digits[6] = '0';
				digits[7] = '\0';

				//printf("digits now = %s\n", digits);

				// We now need to check that there exists a candidate FEB that is
				// made up of the remaining digits
				memset(digitCounts, 0, sizeof(digitCounts));
				memset(rDigits, 0, sizeof(rDigits));
				for (j = 0; j < 7; j++) {
					digitCounts[digits[j] - '0']++;
				}
				c = 0;
				for (j = 0; j < 10; j++) {
					if (!digitCounts[j]) {
						rDigits[c++] = j + '0';
					}
				}

				//printf("Remaining Digits: %c %c %c\n", rDigits[0], rDigits[1], rDigits[2]);

				for (j = 0; j <= m28MaxIdx; j++) {

					//printf("Candidate FEB ... %s\n", mult28[j]);

					f = mult28[j][0];
					e = mult28[j][1];
					b = mult28[j][2];

					if (
						(rDigits[0] == f || rDigits[1] == f || rDigits[2] == f) &&
						(rDigits[0] == e || rDigits[1] == e || rDigits[2] == e) &&
						(rDigits[0] == b || rDigits[1] == b || rDigits[2] == b)
					) {

						printf("%s\n", mult28[j]);

						clock_t toc = clock();
    					printf("%fµs\n", ((double)(toc - tic) / CLOCKS_PER_SEC) * 1000000);

    					return 0;
					}
				}


			}
		}
	}


}
