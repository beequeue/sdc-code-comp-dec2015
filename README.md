# SDC Coding Competition, December 2015

## PHP

PHP solution can be run simply by navigating to the `/php` directory and doing a

```bash
   $ php main.php
```

Run times have been sitting between 250 and 400 microseconds in Scotchbox on my i7 2.4GHz, 8 GB RAM, Windows 7 64bit, laptop.

Intermediate output, during development, was giving me this solution:

	JAN = 465 (/ 31 = 15)
	MAY = 961 (/ 31 = 31)
	APR = 630 (/ 30 = 21)
	FEB = 728 (/ 28 = 26)